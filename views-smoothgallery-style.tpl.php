<?php
/**
 * @file views-smoothgallery-style.tpl.php
 * Default simple view template to display smoothgallery elements.
 *
 * Variables that are available for use include but not limited to the following
 * - $views_smoothgallery_id
 * - $options 
 * - $row   
 * 
 * @ingroup views_templates
 */
    //var_dump($options); exit; 
?>
<!-- Initialize the gallery for this particular gallery display theme file -->
<script type="text/javascript">    
    window.addEvent('domready', 
                        function() {
                                new gallery( $('<?php print $views_smoothgallery_id; ?>'), 
                                                <?php print drupal_json_encode($options); ?> 
                                            );
                        }
    );
</script>

<div id="<?php print $views_smoothgallery_id ?>" > 
      <?php foreach ($rows as $row): ?>
        <?php print $row; ?>
      <?php endforeach; ?>
</div>  

