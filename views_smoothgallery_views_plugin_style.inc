<?php
/**
 * @file
 *  Provide the views zinger slider plugin object with default options and form.
 */

/**
  * Implementation of views_plugin_style().
  */

class views_smoothgallery_views_plugin_style extends views_plugin_style {

    // Set default options
  function option_definition() {
        $options = parent::option_definition();
        unset($options['grouping']);

        $options['vsg_infopane_height'] = array('default' => '80px'); //expose by default in form.
        $options['vsg_width'] = array('default' => '500px'); //expose by default in form.
        $options['vsg_height'] = array('default' => '300px'); //expose by default in form.
        $options['vsg_bg_color'] = array('default' => '#000000'); //expose by default in form.
        $options['vsg_border_color'] = array('default' => '#000000'); //expose by default in form.
        $options['vsg_text_color'] = array('default' => '#ffffff'); //expose by default in form.

        $options['showArrows'] = array('default' => true); //expose by default in form.
        $options['showCarousel'] = array('default' => true); //expose by default in form.
        $options['showInfopane'] = array('default' => true);  //expose by default in form.		
        $options['embedLinks'] = array('default' => true); //expose by default in form.
        $options['fadeDuration'] = array('default' => 500); //expose by default in form.
        $options['timed'] = array('default' => true); //expose by default in form.
        $options['delay'] = array('default' => 9000); //expose by default in form.
        $options['preloader'] = array('default' => true);
        $options['preloaderImage'] = array('default' => true);
        $options['preloaderErrorImage'] = array('default' => true);		
        /* Data retrieval */
        //$options['manualData'] = array('default' => '[]');
        $options['populateFrom'] = array('default' => false);
        $options['populateData'] = array('default' => true);
        $options['destroyAfterPopulate'] = array('default' => true);
        $options['defaultTransition'] = array('default' => 'fade');	//expose by default in form.	
        /* InfoPane options */
        $options['slideInfoZoneOpacity'] = array('default' => 0.7); //expose by default in form.
        $options['slideInfoZoneSlide'] = array('default' => true);        
        /* Carousel options */
        $options['carouselMinimizedOpacity'] = array('default' => 0.4); //expose by default in form.
        $options['carouselMinimizedHeight'] = array('default' => 20); //expose by default in form.		
        $options['carouselMaximizedOpacity'] = array('default' => 0.9); //expose by default in form.        
        $options['thumbHeight'] = array('default' => 75); //expose by default in form. 
        $options['thumbWidth'] = array('default' => 100); //expose by default in form.
        $options['thumbSpacing'] = array('default' => 10); //expose by default in form.
        $options['thumbIdleOpacity'] = array('default' => 0.2);
        $options['textShowCarousel'] = array('default' => 'Featured Content'); //expose by default in form.
        $options['showCarouselLabel'] = array('default' => true);
        $options['thumbCloseCarousel'] = array('default' => true);
        $options['useThumbGenerator'] = array('default' => false);
        $options['thumbGenerator'] = array('default' => 'resizer.php');
        $options['useExternalCarousel'] = array('default' => false);
        $options['carouselElement'] = array('default' => false);
        $options['carouselHorizontal'] = array('default' => true);
        $options['activateCarouselScroller'] = array('default' => true);
        $options['carouselPreloader'] = array('default' => true);
        $options['textPreloadingCarousel'] = array('default' => 'Loading...');		
        /* CSS Classes */
        $options['baseClass'] = array('default' => 'jdGallery');
        $options['withArrowsClass'] = array('default' => 'withArrows');		
        /* Plugins: HistoryManager */
        $options['useHistoryManager'] = array('default' => false);
        $options['customHistoryKey'] = array('default' => false);		
        /* Plugins: ReMooz */
        $options['useReMooz'] = array('default' => false);

        return $options;
  }

  function options_form(&$form, &$form_state) {
      
    // Include ctools dependent support
    //ctools_include('dependent');
      
    parent::options_form($form, $form_state);
    unset($form['grouping']);

    /*
    $presets = array();
    foreach (image_styles() as $p) {
      $presets[$p['name']] = $p['name'];
    }   
    */
    
    $form['textShowCarousel'] = array(
      '#type' => 'textfield',
      '#size' => 25,
      '#title' => t('Carousel caption'),
      '#default_value' => $this->options['textShowCarousel'],
      '#description' => t('Text to display on carousel handle. Defaults to <b>Featured Content</b>.'),
    );
    
    $form['vsg_width'] = array(
      '#type' => 'textfield',
      '#size' => 25,
      '#title' => t('Gallery width'),
      //'#required' => TRUE,
      '#default_value' => $this->options['vsg_width'],
      '#description' => t('Width of gallery. Use any valid css width value. Defaults to <b>500px</b>.'),
    );
    
    $form['vsg_height'] = array(
      '#type' => 'textfield',
      '#size' => 25,
      '#title' => t('Gallery height'),
      //'#required' => TRUE,
      '#default_value' => $this->options['vsg_height'],
      '#description' => t('Height of gallery. Use any valid css height value. Defaults to <b>300px</b>.'),
    );
    
    $form['vsg_infopane_height'] = array(
      '#type' => 'textfield',
      '#size' => 25,
      '#title' => t('Information pane height'),
      //'#required' => TRUE,
      '#default_value' => $this->options['vsg_infopane_height'],
      '#description' => t('Height of information pane. Use any valid css height value. Defaults to <b>80px</b>.'),
    );
    
    $form['other_vsg_settings'] = array(
        '#type' => 'fieldset',
        '#title' => t('Views SmoothGallery other settings'),       
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
    );    
    
    $form['other_vsg_settings']['vsg_bg_color'] = array(
      '#type' => 'textfield',
      '#size' => 25,
      '#title' => t('Gallery background color'),
      //'#required' => TRUE,
      '#default_value' => $this->options['vsg_bg_color'],
      '#description' => t('Gallery background color. Use any valid css color value. Defaults to <b>#000000</b>.'),
    );    
    
    $form['other_vsg_settings']['vsg_border_color'] = array(
      '#type' => 'textfield',
      '#size' => 25,
      //'#required' => TRUE,
      '#title' => t('Gallery border color'),
      '#default_value' => $this->options['vsg_border_color'],
      '#description' => t('Gallery border color. Use any valid css color value. Defaults to <b>#000000</b>.'),
    );
    
    $form['other_vsg_settings']['vsg_text_color'] = array(
      '#type' => 'textfield',
      '#size' => 25,
      //'#required' => TRUE,
      '#title' => t('Gallery text color'),
      '#default_value' => $this->options['vsg_text_color'],
      '#description' => t('Gallery text color. Use any valid css color value. Defaults to <b>#ffffff</b>.'),
    );
    
    $form['other_vsg_settings']['thumbHeight'] = array(
      '#type' => 'textfield',
      '#size' => 25,
      //'#required' => TRUE,
      '#title' => t('Carousel thumbnail height'),
      '#default_value' => $this->options['thumbHeight'],
      '#description' => t('Gallery carousel thumbnail height. Defaults to <b>75</b>.'),
    );
    
    $form['other_vsg_settings']['thumbWidth'] = array(
      '#type' => 'textfield',
      '#size' => 25,
      //'#required' => TRUE,
      '#title' => t('Carousel thumbnail width'),
      '#default_value' => $this->options['thumbWidth'],
      '#description' => t('Gallery carousel thumbnail width. Defaults to <b>100</b>.'),
    );
    
    $form['other_vsg_settings']['thumbSpacing'] = array(
      '#type' => 'textfield',
      '#size' => 25,
      //'#required' => TRUE,
      '#title' => t('Carousel thumbnail spacing'),
      '#default_value' => $this->options['thumbSpacing'],
      '#description' => t('Gallery carousel thumbnail spacing. Defaults to <b>10</b>.'),
    );    
    
    $form['other_vsg_settings']['showArrows'] = array(
      '#type' => 'select',      
      '#title' => t('Show gallery arrows'),
      '#default_value' => $this->options['showArrows'],
      '#options' => array(true => 'Yes', false => 'No'),      
      '#description' => t('Whether or not to show gallery navigation arrows.'),
    );
    
    $form['other_vsg_settings']['showCarousel'] = array(
      '#type' => 'select',
      '#title' => t('Show gallery carousel'),
      '#default_value' => $this->options['showCarousel'],
      '#options' => array(true => 'Yes', false => 'No'),      
      '#description' => t('Whether or not to show gallery carousel.'),
    );
    
    $form['other_vsg_settings']['showInfopane'] = array(
      '#type' => 'select',
      '#title' => t('Show gallery information pane'),
      '#default_value' => $this->options['showInfopane'],
      '#options' => array(true => 'Yes', false => 'No'),      
      '#description' => t('Whether or not to show gallery information pane.'),
    );
    
    $form['other_vsg_settings']['embedLinks'] = array(
      '#type' => 'select',
      '#title' => t('Link gallery image'),
      '#default_value' => $this->options['embedLinks'],
      '#options' => array(true => 'Yes', false => 'No'),      
      '#description' => t('Whether or not to embed gallery links.'),
    );    

    $form['other_vsg_settings']['fadeDuration'] = array(
      '#type' => 'textfield',
      '#size' => 25,
      //'#required' => TRUE,
      '#title' => t('Fade duration'),
      '#default_value' => $this->options['fadeDuration'],
      '#description' => t('Gallery image fade druration in milliseconds. Defaults to <b>500</b>.'),
    );
    
    $form['other_vsg_settings']['timed'] = array(
      '#type' => 'select',
      '#title' => t('Gallery autoplay'),
      '#default_value' => $this->options['timed'],
      '#options' => array(true => 'Yes', false => 'No'),      
      '#description' => t('Whether or not galllery should autoplay.'),
    );
    
    $form['other_vsg_settings']['delay'] = array(
      '#type' => 'textfield',
      '#size' => 25,      
      '#title' => t('Image duration'),
      '#default_value' => $this->options['delay'],
      '#description' => t('Time in milliseconds for an image to display. Defaults to <b>9000</b>.'),
    );
    
    $form['other_vsg_settings']['defaultTransition'] = array(
      '#type' => 'select',
      '#title' => t('Gallery transition style'),
      '#default_value' => $this->options['defaultTransition'],
      '#options' => array('fade' => 'Simple Fade', 'fadeslideleft' => 'Fade Slide Left',
          'continuoushorizontal' => 'Continuous Horizontal', 'continuousvertical' => 'Continuous Vertical',
          'useReMooz' => 'Zoom',
      ),      
      '#description' => t('Style for use between image transition in gallery.'),
    );

    $form['other_vsg_settings']['slideInfoZoneOpacity'] = array(
      '#type' => 'textfield',
      '#size' => 25,
      '#title' => t('Information pane opacity'),
      '#default_value' => $this->options['slideInfoZoneOpacity'],
      '#description' => t('Opacity of information pane. Defaults to <b>0.7</b>.'),
    );
    
    $form['other_vsg_settings']['carouselMinimizedOpacity'] = array(
      '#type' => 'textfield',
      '#size' => 25,
      '#title' => t('Minimized carousel opacity'),
      '#default_value' => $this->options['carouselMinimizedOpacity'],
      '#description' => t('Opacity of minimized carousel. Defaults to <b>0.4</b>.'),
    );
    
    $form['other_vsg_settings']['carouselMaximizedOpacity'] = array(
      '#type' => 'textfield',
      '#size' => 25,
      '#title' => t('Maximized carousel opacity'),
      '#default_value' => $this->options['carouselMaximizedOpacity'],
      '#description' => t('Opacity of maximized carousel. Defaults to <b>0.9</b>.'),
    );
    
    $form['other_vsg_settings']['carouselMinimizedHeight'] = array(
      '#type' => 'textfield',
      '#size' => 25,
      '#title' => t('Carousel minimized height'),
      '#default_value' => $this->options['carouselMinimizedHeight'],
      '#description' => t('Carousel minimized height. Defaults to <b>20</b>.'),
    );
    
  }  
  
}
