<?php
/**
 * @file
 * Contains the base row style plugin.
 */

/**
 * The basic 'fields' row plugin
 *
 * This displays fields one after another, giving options for inline or not.
 *
 * @ingroup views_row_plugins
 */
class views_smoothgallery_views_plugin_row extends views_plugin_row {
  
function option_definition() {
    $options = parent::option_definition();
    
    $options['vsg_image_field'] = array('default' => '');
    $options['vsg_infopane_title_field'] = array('default' => '');
    $options['vsg_infopane_desc_field'] = array('default' => '');
    $options['vsg_link_field'] = array('default' => '');
    
    return $options;
    
  }
  
  function options_form(&$form, &$form_state) {
    
    parent::options_form($form, $form_state);
    
    // Pre-build all of our option lists for the dials and switches that follow.
    $fields = array();
    foreach ($this->display->handler->get_handlers('field') as $field => $handler) {
      $label = $handler->label();
      if ($label) {
        $fields[$field] = $label;
      }
      else {
        $fields[$field] = $handler->ui_name();
      }
    }
   
    $form['vsg_image_field'] = array(
      '#type' => 'select',
      '#title' => t('Gallery image field'),
      '#required' => TRUE,
      '#options' => $fields,
      '#default_value' => $this->options['vsg_image_field'],
      '#description' => t('Select the field that will be used as the gallery image field.
          Output is expected to be an img markup element.'),
    );

    $form['vsg_infopane_title_field'] = array(
      '#type' => 'select',
      '#title' => t('Information pane title field'),
      '#required' => TRUE,
      '#options' => $fields,
      '#default_value' => $this->options['vsg_infopane_title_field'],
      '#description' => t('Select the field that will be used as the information pane title field.
          Output is expected to be plain text markup.'),
    );
    
    $form['vsg_infopane_desc_field'] = array(
      '#type' => 'select',
      '#title' => t('Information pane description field'),
      '#required' => TRUE,
      '#options' => $fields,
      '#default_value' => $this->options['vsg_infopane_desc_field'],
      '#description' => t('Select the field that will be used as the information
          pane description field. Output is expected to be a markup with only inline elements.'),
    );
 
    $form['vsg_link_field'] = array(
      '#type' => 'select',
      '#title' => t('Link field'),
      '#required' => TRUE,
      '#options' => $fields,
      '#default_value' => $this->options['vsg_link_field'],
      '#description' => t('Select the field that will be used as the link field.
          Output is expected to be a single anchor element markup.'),
    );
    
  }
  
}
