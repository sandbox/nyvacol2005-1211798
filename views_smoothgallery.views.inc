<?php

/**
 *  Provide the views zinger slider plugin definition.
 */

function views_smoothgallery_views_plugins() {  
    return array(
    'module' => 'views_smoothgallery',
    'style' => array(
      'views_smoothgallery' => array(
        'title' => t('Smooth Gallery'),
        'help' => t("Display a view with JonDesign's SmoothGallery."),
        'handler' => 'views_smoothgallery_views_plugin_style',
        'uses options' => TRUE,
        'uses row plugin' => TRUE,
        'uses grouping' => FALSE,
        'type' => 'normal',
        //'parent' => 'list',
        'path' => drupal_get_path('module', 'views_smoothgallery'),  
        'theme' => 'views_smoothgallery_style',
        //'theme path' => drupal_get_path('module', 'views_zinger_slider') . '/theme',
        //'theme file' => 'views_zinger_slider.theme.inc',
      ),        
    ),
      
    'row' => array(
      'smoothgallery_fields' => array(
        'title' => t('Smooth Gallery Element'),
        'help' => t('Choose the fields to display using Smooth Gallery.'),
        'handler' => 'views_smoothgallery_views_plugin_row',
        'path' => drupal_get_path('module', 'views_smoothgallery'),
        'theme' => 'views_smoothgallery_row',
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
      ),        
    ),
  );
}