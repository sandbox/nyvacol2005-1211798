<?php
/**
 * @file views-smoothgallery-row.tpl.php
 * Main row view template
 *
 * - $style_options
 * - $options
 * - $vsg: Array containing gallery image and thumb as in 
 *    - $vsg['image']   // Smooth Gallery Image Element
 *    - $vsg['thumbimage'] // Smooth Gallery Thumbnail Image element 
 *    - $vsg['title']      // Smooth Gallery Title
 *    - $vsg['info_pane_desc'] // Smooth Gallery subtitle description
 *    - $vsg['link_url']   // Smooth Gallery url for link element 
 *
 * @ingroup views_templates
 */
?>

<div class="imageElement"> 
	<h3><?php print $vsg['title']; ?></h3>
	<p><?php print $vsg['info_pane_desc']; ?></p>
	<a href="<?php print $vsg['link_url']; ?>" title="<?php print $vsg['title']; ?>" class="open"></a>    
	<?php print $vsg['image']; ?>	
</div>